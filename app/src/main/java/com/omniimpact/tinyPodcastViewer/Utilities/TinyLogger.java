package com.omniimpact.tinyPodcastViewer.Utilities;

import android.util.Log;

/**
 * Created by daniel on 2/20/18.
 * Simple logging utility.
 * In LogCat, filter by » to see just these logs if your device throws lots of extra ones.
 * Note: Using Log.i here instead of Log.d due to some devices** not displaying Log.d
 * **I apparently have a weird test device.
 */

public class TinyLogger {

    private static final String ACTION_NORMAL = "--»";
    private static final String ACTION_ASYNC = "..»";

    private static String tagName(Class caller){
        return "["+caller.getSimpleName()+"]";
    }

    public static void doing(Class caller, String method, String message){
        Log.i(tagName(caller), ACTION_NORMAL+" ("+method+") "+message);
    }

    public static void doing(Class caller, String method){
        Log.i(tagName(caller), ACTION_NORMAL+" ("+method+")");
    }

    public static void async(Class caller, String method, String message){
        Log.i(tagName(caller), ACTION_ASYNC+" ("+method+") "+message);
    }

    public static void async(Class caller, String method){
        Log.i(tagName(caller), ACTION_ASYNC+" ("+method+")");
    }


}
