package com.omniimpact.tinyPodcastViewer.Utilities;

/**
 * Created by daniel on 2/20/18.
 */

public interface IOnSignal {

    void onSignal(Constants.SIGNALS signal);

}
