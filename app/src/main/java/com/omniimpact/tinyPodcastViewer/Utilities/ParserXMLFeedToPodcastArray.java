package com.omniimpact.tinyPodcastViewer.Utilities;

import android.text.Html;
import android.text.Spanned;

import com.omniimpact.tinyPodcastViewer.Models.Podcast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Utility to help parse the XML feed to an array of simple objects.
 * Created by daniel on 2/20/18.
 * Parse out the relevant following items:
 * title, link, author, pubDate, guid, description, enclosure, thr:total,
 * media:content, itunes:explicit, itunes:subtitle, itunes:author, itunes:summary,
 * itunes:keywords, feedburner:origLink
 */

public class ParserXMLFeedToPodcastArray {

    private ArrayList<Podcast> mPodcasts;

    public ParserXMLFeedToPodcastArray(){
        mPodcasts = new ArrayList<>();
    }

    public ParserXMLFeedToPodcastArray parseUrl(String url){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new URL(url).openStream());
            document.getDocumentElement().normalize();
            NodeList items = document.getElementsByTagName(Constants.XMLKeys.KEY_ITEM);
            TinyLogger.doing(getClass(), "parseUrl", "Found items: "+items.getLength());
            for (int i = 0; i < items.getLength(); i++) {
                Element currentElement = (Element) items.item(i);
                mPodcasts.add(getPodcastFromItem(currentElement));
            }

        } catch (ParserConfigurationException|SAXException|IOException e) {
            e.printStackTrace();
        }
        TinyLogger.doing(getClass(), "parseUrl", "Completed parsing "+mPodcasts.size()+" podcasts.");
        return this;
    }

    private Podcast getPodcastFromItem(Element itemElement){

        Podcast podcast = new Podcast();

        // Get the title
        NodeList titleElements = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_TITLE);
        if(titleElements.getLength() > 0){
            podcast.setTitle(
                    StringUtilities.handleDoubleEncodedStringsWithQuirks(titleElements.item(0).getTextContent())
            );
        }

        // Get the date string. for the purposes of this app, this should be good enough
        NodeList dateElements = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_PUBDATE);
        if(dateElements.getLength() > 0) podcast.setPubDateString(dateElements.item(0).getTextContent());

        // Get the content URL. This one, the URL is an attribute.
        NodeList contentUrls = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_AUDIO_ENCLOSURE);
        if(contentUrls.getLength() > 0){
            podcast.setContentUrl( ((Element)contentUrls.item(0)).getAttribute(Constants.XMLKeys.KEY_ATTR_URL) );
        }

        // Finding artwork is more tricky. It may be stored under itunes:image OR media:thumbnail
        // If we can find a thumbnail, this is preferred, so we use that first. If the thumbnail
        // is from blogspot, we can increase it to an appropriate size and use it as the image.
        // Next, we look for the itunes:image tag. If we have it, we can use this in either case
        // that we still have empty values.

        String thumbnailUrlString = null;
        String imageUrlString = null;
        NodeList thumbnailUrls = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_MEDIA_THUMBNAIL);
        if(thumbnailUrls.getLength() > 0){
            thumbnailUrlString = ((Element)thumbnailUrls.item(0)).getAttribute(Constants.XMLKeys.KEY_ATTR_URL);
            if(thumbnailUrlString.contains("blogspot.com") && thumbnailUrlString.contains("s72-c")){
                imageUrlString = thumbnailUrlString.replace("s72-c","s640-c");
            }
        }
        if(thumbnailUrlString == null || imageUrlString == null){
            NodeList imageUrls = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_ITUNES_IMAGE);
            if(imageUrls.getLength() > 0){
                // The itunes image is under "href"
                imageUrlString = ((Element)imageUrls.item(0)).getAttribute(Constants.XMLKeys.KEY_ATTR_HREF);
            }
            if(imageUrlString != null && thumbnailUrlString == null){
                thumbnailUrlString = imageUrlString;
            }
        }
        podcast.setThumbnailUrl(thumbnailUrlString);
        podcast.setImageUrl(imageUrlString);

        // Get the episode itunes:summary
        NodeList summaries = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_ITUNES_SUMMARY);
        if(summaries.getLength() > 0){
            podcast.setSummary(
                    StringUtilities.handleDoubleEncodedStringsWithQuirks(summaries.item(0).getTextContent())
            );
        }

        // Get the description
        NodeList descriptions = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_DESCRIPTION);
        if(descriptions.getLength() > 0){
            podcast.setDescription(
                    StringUtilities.handleDoubleEncodedStringsWithQuirks(descriptions.item(0).getTextContent())
            );
        }

        // Get the keywords
        NodeList keywords = itemElement.getElementsByTagName(Constants.XMLKeys.KEY_ITUNES_KEYWORDS);
        if(keywords.getLength() > 0){
            podcast.setKeywords( keywords.item(0).getTextContent() );
        }

        return podcast;
    }

    public ArrayList<Podcast> getPodcasts(){
        return mPodcasts;
    }

}
