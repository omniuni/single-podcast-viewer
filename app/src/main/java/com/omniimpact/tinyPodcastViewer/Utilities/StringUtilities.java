package com.omniimpact.tinyPodcastViewer.Utilities;

import android.os.Build;
import android.text.Html;

/**
 * Created by daniel on 2/22/18.
 */

public class StringUtilities {

    public static String handleDoubleEncodedStringsWithQuirks(String input){
        String stringPass1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stringPass1 = Html.fromHtml(input, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH).toString();
        }else{
            stringPass1 = Html.fromHtml(input).toString();
        }
        // Since it appears to be encoded twice, we do this again;
        String stringPass2;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stringPass2 = Html.fromHtml(stringPass1, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH).toString();
        }else{
            stringPass2 = Html.fromHtml(stringPass1).toString();
        }
        // Now we handle a few quirks...
        // For some reason,  some subtitles start with a headline, followed by "In this episode"
        // but no space between. This sometimes results in Android creating something.in hyperlinks.
        // At least in this case, we will try to fix it.
        return stringPass2.replace(".In",". \r\nIn");
    }

}
