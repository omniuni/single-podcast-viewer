package com.omniimpact.tinyPodcastViewer.Utilities;

import com.omniimpact.tinyPodcastViewer.Models.Podcast;

import java.util.ArrayList;

/**
 * Created by daniel on 2/21/18.
 */

public class PodcastArrayListManagement {

    public static synchronized void filterPodcasts(){
        ApplicationState state = ApplicationState.getInstance();
        ArrayList<Podcast> podcastsToFilter = state.getPodcasts();
        String filterBy = state.getFilter().toLowerCase();
        if(!filterBy.equals("")) {
            ArrayList<Podcast> filteredPodcasts = new ArrayList<>();
            for (int i = 0; i < podcastsToFilter.size(); i++) {
                Podcast checkPodcast = podcastsToFilter.get(i);
                if(
                    checkPodcast.getTitle().toLowerCase().contains(filterBy) ||
                    checkPodcast.getDescription().toLowerCase().contains(filterBy) ||
                    checkPodcast.getSummary().toLowerCase().contains(filterBy) ||
                    checkPodcast.getKeywords().toLowerCase().contains(filterBy)
                ){
                    filteredPodcasts.add(checkPodcast);
                }
            }
            state.setFilteredPodcasts(filteredPodcasts);
        }else{
            state.setFilteredPodcasts(podcastsToFilter);
        }

    }

}
