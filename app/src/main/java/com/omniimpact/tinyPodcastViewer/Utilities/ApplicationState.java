package com.omniimpact.tinyPodcastViewer.Utilities;

import com.omniimpact.tinyPodcastViewer.Models.Podcast;

import java.util.ArrayList;

/**
 * Created by daniel on 2/20/18.
 * Create a Singleton that holds the state of the application.
 * For the purposes of this application, it will generally hold
 * a parsed list of Podcast objects. It may also hold useful values
 * such as the current list filter.
 */

public class ApplicationState {

    private static ApplicationState mInstance;
    private ArrayList<Podcast> mPodcasts;
    private String mFilter;
    private ArrayList<Podcast> mFilteredPodcasts;
    private String mPodcastUrl;

    public static synchronized ApplicationState getInstance(){
        if(mInstance == null) mInstance = new ApplicationState();
        return mInstance;
    }

    public void setPodcastUrl(String podcastUrl){
        mPodcastUrl = podcastUrl;
    }

    public String getPodcastUrl(){
        return mPodcastUrl;
    }

    public void setPodcasts(ArrayList<Podcast> podcasts){
        mPodcasts = podcasts;
    }

    public ArrayList<Podcast> getPodcasts(){
        return mPodcasts;
    }

    public void setFilter(String filter){
        mFilter = filter;
    }

    public String getFilter(){
        return mFilter == null ? "" : mFilter;
    }

    public void setFilteredPodcasts(ArrayList<Podcast> filteredPodcasts){
        mFilteredPodcasts = filteredPodcasts;
    }

    public ArrayList<Podcast> getFilteredPodcasts(){
        return mFilteredPodcasts;
    }

}
