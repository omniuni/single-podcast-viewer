package com.omniimpact.tinyPodcastViewer.Utilities;

/**
 * Hold various constant values used throughout the application.
 * Created by daniel on 2/21/18.
 */

public class Constants {

    public enum SIGNALS{
        PARSE_COMPLETE
    }

    class XMLKeys{
        static final String KEY_ITEM = "item";
        static final String KEY_TITLE = "title";
        static final String KEY_PUBDATE = "pubDate";
        static final String KEY_AUDIO_ENCLOSURE = "enclosure";
        static final String KEY_MEDIA_THUMBNAIL = "media:thumbnail";
        static final String KEY_ITUNES_SUMMARY = "itunes:summary";
        static final String KEY_DESCRIPTION = "description";
        static final String KEY_ITUNES_KEYWORDS = "itunes:keywords";
        static final String KEY_ITUNES_IMAGE = "itunes:image";

        static final String KEY_ATTR_URL = "url";
        static final String KEY_ATTR_HREF = "href";
    }

}
