package com.omniimpact.tinyPodcastViewer.UserInterface;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;

import com.omniimpact.tinyPodcastViewer.Async.AsyncGetFeed;
import com.omniimpact.tinyPodcastViewer.R;
import com.omniimpact.tinyPodcastViewer.Utilities.ApplicationState;
import com.omniimpact.tinyPodcastViewer.Utilities.Constants;
import com.omniimpact.tinyPodcastViewer.Utilities.IOnSignal;
import com.omniimpact.tinyPodcastViewer.Utilities.PodcastArrayListManagement;
import com.omniimpact.tinyPodcastViewer.Utilities.TinyLogger;

public class ActivityMain extends AppCompatActivity implements IOnSignal{

    private ApplicationState mApplicationStateReference;
    private ArrayAdapterPodcastList mPodcastArrayAdapter;

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TinyLogger.doing(getClass(), "onCreate");
        mApplicationStateReference = ApplicationState.getInstance();
        new AsyncGetFeed(this).execute();
        setContentView(R.layout.activity_main);
    }

    private void updateUi(){

        ListView lv = findViewById(R.id.id_list_items);
        mPodcastArrayAdapter = new ArrayAdapterPodcastList(this, R.layout.layout_list_podcasts);
        lv.setAdapter(mPodcastArrayAdapter);

        EditText etFilter = findViewById(R.id.id_et_filter);
        etFilter.addTextChangedListener(new FilterTextwatcher());

    }

    @Override
    public void onSignal(Constants.SIGNALS signal) {
        TinyLogger.doing(getClass(), "onSignal", signal.toString()+" for "+mApplicationStateReference.getPodcasts().size()+" podcasts.");
        updateUi();
    }

    private class FilterTextwatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            TinyLogger.doing(getClass(), "afterTextChanged");
            String newText = editable.toString();
            mApplicationStateReference.setFilter(newText);
            PodcastArrayListManagement.filterPodcasts();
            mPodcastArrayAdapter.notifyDataSetChanged();
        }
    }

}
