package com.omniimpact.tinyPodcastViewer.UserInterface;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omniimpact.tinyPodcastViewer.Models.Podcast;
import com.omniimpact.tinyPodcastViewer.R;
import com.omniimpact.tinyPodcastViewer.Utilities.ApplicationState;
import com.omniimpact.tinyPodcastViewer.Utilities.TinyLogger;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * ArrayAdapter that displays the filtered podcasts.
 * Created by daniel on 2/21/18.
 */

public class ArrayAdapterPodcastList extends ArrayAdapter{

    private class ViewHolder{
        private TextView title;
        private TextView subTitle;
        private ImageView icon;
        private View view;

        public TextView getTitle() {
            return title;
        }

        public void setTitle(TextView title) {
            this.title = title;
        }

        TextView getSubTitle() {
            return subTitle;
        }

        void setSubTitle(TextView subTitle) {
            this.subTitle = subTitle;
        }

        public ImageView getIcon() {
            return icon;
        }

        public void setIcon(ImageView icon) {
            this.icon = icon;
        }

        public View getView() {
            return view;
        }

        public void setView(View view) {
            this.view = view;
        }
    }

    private int mResource;
    private AppCompatActivity mActivity;

    ArrayAdapterPodcastList(Context context, int resource) {
        super(context, resource);
        mActivity = (AppCompatActivity) context;
        mResource = resource;
    }

    @Override
    public int getCount() {
        return ApplicationState.getInstance().getFilteredPodcasts().size();
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh;
        if(convertView != null && convertView.getTag() != null){
            vh = (ViewHolder) convertView.getTag();
        }else{
            vh = new ViewHolder();
            vh.setView(mActivity.getLayoutInflater().inflate(mResource, parent, false));
            vh.setTitle((TextView) vh.getView().findViewById(R.id.id_tv_title));
            vh.setSubTitle((TextView) vh.getView().findViewById(R.id.id_tv_date));
            vh.setIcon((ImageView) vh.getView().findViewById(R.id.id_iv_thumbnail));
        }

        final Podcast podcast = ApplicationState.getInstance().getFilteredPodcasts().get(position);
        vh.getTitle().setText(podcast.getTitle());
        vh.getSubTitle().setText(podcast.getPubDateString());
        Picasso.with(mActivity).load(podcast.getThumbnailUrl()).
            fit().centerCrop().
            placeholder(R.drawable.ic_launcher).into(vh.getIcon());
        vh.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetailsDialogue(podcast);
            }
        });

        return vh.getView();

    }

    private void showDetailsDialogue(final Podcast podcast){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setView(getPodcastDialogView(podcast));
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("Play Podcast", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                TinyLogger.doing(getClass(), "setPositiveButton onClick", "Play: "+podcast.getContentUrl());
                Uri intentUri = Uri.parse(podcast.getContentUrl());
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(intentUri, "audio/mp3");
                mActivity.startActivity(intent);
            }
        });
        builder.create().show();
    }

    private View getPodcastDialogView(Podcast podcast){
        final View podcastDetailView = mActivity.getLayoutInflater().inflate(R.layout.dialog_podcast, null);

        // Set up the header by applying the image and title text,
        // and a callback from Picasso to adjust the coloration.
        final ImageView ivThumbnail = podcastDetailView.findViewById(R.id.id_iv_thumbnail);
        final RelativeLayout headerLayout = podcastDetailView.findViewById(R.id.id_frame_header);
        final TextView tvTitle = podcastDetailView.findViewById(R.id.id_tv_title);
        tvTitle.setText(podcast.getTitle());

        // TODO: Consider separating Picasso callback
        // NOTE: Although it would make the code nicer, this is a purely optional feature,
        // so as long as it's not hurting anything, and this method isn't too long,
        // it can stay here for now. If this method gets too long, the callback can be separated.
        if(podcast.getThumbnailUrl() != null){
            Picasso.with(mActivity).load(podcast.getImageUrl()).
                placeholder(R.drawable.ic_launcher).
                fit().centerCrop().
                into(
                    ivThumbnail,
                    new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap ivBitmap = ((BitmapDrawable)ivThumbnail.getDrawable()).getBitmap();
                            Palette.from(ivBitmap).generate(new Palette.PaletteAsyncListener() {
                                @Override
                                public void onGenerated(@NonNull Palette palette) {
                                    headerLayout.setBackgroundColor(palette.getDarkMutedColor(Color.GRAY));
                                    tvTitle.setTextColor(palette.getLightVibrantColor(Color.WHITE));
                                }
                            });
                        }

                        @Override
                        public void onError() {
                            podcastDetailView.findViewById(R.id.id_iv_cropper).setVisibility(View.GONE);
                        }
                    }
                );
        }else{
            podcastDetailView.findViewById(R.id.id_iv_cropper).setVisibility(View.GONE);
        }

        // The technically correct item to get the summary is the "description", but on some XML
        // feeds this has encoding problems. We will therefore prefer the itunes:summary, and
        // if it is empty, use the description. This tends to yield the better encoded option.
        TextView tvSummary = podcastDetailView.findViewById(R.id.id_tv_summary);
        String summaryText = (podcast.getSummary().trim().isEmpty()) ? podcast.getDescription() : podcast.getSummary();
        tvSummary.setText(summaryText);

        TextView tvKeywords = podcastDetailView.findViewById(R.id.id_tv_keywords);
        tvKeywords.setText(podcast.getKeywords());

        TextView tvDate = podcastDetailView.findViewById(R.id.id_tv_date);
        tvDate.setText(podcast.getPubDateString());

        return podcastDetailView;
    }

}
