package com.omniimpact.tinyPodcastViewer.Async;

import android.os.AsyncTask;

import com.omniimpact.tinyPodcastViewer.Utilities.ApplicationState;
import com.omniimpact.tinyPodcastViewer.Utilities.Constants;
import com.omniimpact.tinyPodcastViewer.Utilities.IOnSignal;
import com.omniimpact.tinyPodcastViewer.Utilities.ParserXMLFeedToPodcastArray;
import com.omniimpact.tinyPodcastViewer.Utilities.PodcastArrayListManagement;
import com.omniimpact.tinyPodcastViewer.Utilities.TinyLogger;

/**
 * AsyncTask to fetch the list of podcasts.
 * Should also apply the initial filter.
 * Created by daniel on 2/20/18.
 */

public class AsyncGetFeed extends AsyncTask {

    private IOnSignal mOnSignalCaller;

    public AsyncGetFeed(IOnSignal caller){
        mOnSignalCaller = caller;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        TinyLogger.async(getClass(), "doInBackground");
        ParserXMLFeedToPodcastArray parser = new ParserXMLFeedToPodcastArray().parseUrl(ApplicationState.getInstance().getPodcastUrl());
        ApplicationState.getInstance().setPodcasts(parser.getPodcasts());
        PodcastArrayListManagement.filterPodcasts();
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        TinyLogger.async(getClass(), "onPostExecute", "Completed loading Podcast feed.");
        mOnSignalCaller.onSignal(Constants.SIGNALS.PARSE_COMPLETE);
    }

}
