package com.omniimpact.tinyPodcastViewer;

import android.app.Application;

import com.omniimpact.tinyPodcastViewer.Utilities.ApplicationState;
import com.omniimpact.tinyPodcastViewer.Utilities.TinyLogger;

/**
 * Created by daniel on 2/20/18.
 * Note when the application starts.
 */

public class ApplicationMain extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TinyLogger.doing(getClass(), "Application Initializing...");
        ApplicationState.getInstance().setPodcastUrl(getResources().getString(R.string.feed_url));
    }
}
