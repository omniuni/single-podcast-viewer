package com.omniimpact.tinyPodcastViewer.Models;

/**
 * Model for a Podcast
 * Created by daniel on 2/20/18.
 */

public class Podcast {

    private String mTitle = "";
    private String mPubDateString;
    private String mContentUrl;
    private String mThumbnailUrl;
    private String mSummary = "";
    private String mDescription = "";
    private String mImageUrl;
    private String mKeywords = "";

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setPubDateString(String pubDateString){
        mPubDateString = pubDateString;
    }

    public String getPubDateString(){
        return mPubDateString;
    }

    public void setContentUrl(String contentUrl){
        mContentUrl = contentUrl;
    }

    public String getContentUrl(){
        return mContentUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        mThumbnailUrl = thumbnailUrl;
    }

    public String getThumbnailUrl(){
        return mThumbnailUrl;
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public String getSummary(){
        return mSummary;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl(){
        return mImageUrl;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescription(){
        return mDescription;
    }

    public void setKeywords(String keywords) {
        mKeywords = keywords;
    }

    public String getKeywords(){
        return mKeywords;
    }

}
